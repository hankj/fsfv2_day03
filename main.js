var express = require("express");

var app = express();

app.use(function(req, res, next){
   console.info("Incoming Msg: %s", req.originalUrl);
   next();
});

app.get("/time", function(req, res, next){
    var currTime =  Date();
    //set status
    res.status(202);
    //set file type
    res.type("text/html");
    //set customed header
    res.set("X-My-Header","hello");
    //send response
    res.send("<h1>The current time is: " + currTime + "</h1>");
});

app.use(express.static(__dirname + "/public"));

app.use(function(req, res){
    res.redirect("/error.html");
});

app.set("port", process.argv[2] || process.APP_PORT || 3000);

app.listen(app.get("port"), function(){
    console.info("Web server started on %d", app.get("port"));
});
